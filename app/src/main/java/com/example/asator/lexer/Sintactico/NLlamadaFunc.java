package com.example.asator.lexer.Sintactico;

import android.content.Context;
import android.util.Log;

import com.example.asator.lexer.MainActivity;

/**
 * Created by mfrausto on 11/27/16.
 */

public class NLlamadaFunc extends Nodo {
    NIdentificador identificador;
    Nodo argumentos;

    public NLlamadaFunc(Sintactico pila) {
        pila.pop();//Estado
        pila.pop();//)

        pila.pop();//Estado
        this.argumentos = pila.pop().getNodo();

        pila.pop();//Estado
        pila.pop();//(

        this.identificador = new NIdentificador(pila, null);
        this.simbolo = "LlamadaFunc";
    }

    public void muestra(Context context) {
        obtieneSangria();
        Log.e("Semantico", this.sangria+"<LlamadaFunc>");
        ((MainActivity)context).muestraArbol(this.sangria+"<LlamadaFunc>");

        this.tamSangria++;
        if(argumentos != null)
            argumentos.muestra(context);
        if(identificador != null)
            identificador.muestra(context);
        this.tamSangria--;

        if(sig != null)
            sig.muestra(context);
    }
}
