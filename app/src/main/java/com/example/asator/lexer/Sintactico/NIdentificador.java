package com.example.asator.lexer.Sintactico;

import android.content.Context;
import android.util.Log;

import com.example.asator.lexer.MainActivity;

/**
 * Created by mfrausto on 11/21/16.
 */

public class NIdentificador extends NExpresion {
    public NIdentificador(Sintactico pila, Nodo sig) {
        pila.pop();//Estado
        this.simbolo = pila.pop().getSimbolo();
        this.sig = sig;
    }

    @Override
    public void muestra(Context context) {
        obtieneSangria();
        Log.e("Semantico", this.sangria+"<Identificador> ::= "+this.simbolo);
        ((MainActivity)context).muestraArbol(this.sangria+"<Identificador> ::= "+this.simbolo);

        if(sig != null)
            sig.muestra(context);
    }
}
