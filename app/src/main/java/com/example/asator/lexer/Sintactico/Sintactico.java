package com.example.asator.lexer.Sintactico;

import android.util.Log;

import java.util.Iterator;

/**
 * Created by asator on 8/29/16.
 */
public class Sintactico extends AbstractSintactico {
    @Override
    public void push(ElementoPila x) {
        this.lista.push(x);
    }

    @Override
    public ElementoPila top() {
        return this.lista.peek();
    }

    @Override
    public ElementoPila pop() {
        return this.lista.pop();
    }

    @Override
    public String muestra() {
        Iterator<ElementoPila> iterator = this.lista.iterator();
        String pila = "";

        while (iterator.hasNext()){
            ElementoPila thisEle = iterator.next();
            String elePila = (thisEle.getSimbolo() != null)? thisEle.getSimbolo() : thisEle.getId().toString();
            pila += " "+elePila;
        }

        Log.e("PILA", pila);
        return pila;
    }
}
