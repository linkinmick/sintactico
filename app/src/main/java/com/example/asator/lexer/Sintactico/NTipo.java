package com.example.asator.lexer.Sintactico;

import android.content.Context;
import android.util.Log;

import com.example.asator.lexer.MainActivity;

/**
 * Created by mfrausto on 11/26/16.
 */

public class NTipo extends Nodo {
    public NTipo(Sintactico pila){
        pila.pop();
        this.simbolo = pila.pop().getSimbolo();
        this.sig = null;
    }

    public Character dameTipo(){
        Character tipo = null;
        if(simbolo.equals("int")){ tipo = 'i'; }
        if(simbolo.equals("float")){ tipo = 'f'; }
        if(simbolo.equals("string")){ tipo = 's'; }
        if(simbolo.equals("void")){ tipo = 'v'; }

        return tipo;
    }

    @Override
    public void muestra(Context context) {
        obtieneSangria();
        Log.e("Semantico", this.sangria+"<Tipo> ::= "+this.simbolo);
        ((MainActivity)context).muestraArbol(this.sangria+"<Tipo> ::= "+this.simbolo);
    }
}
