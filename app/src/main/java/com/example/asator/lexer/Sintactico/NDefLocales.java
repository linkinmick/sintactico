package com.example.asator.lexer.Sintactico;

import android.content.Context;
import android.util.Log;

import com.example.asator.lexer.MainActivity;

/**
 * Created by mfrausto on 11/27/16.
 */

public class NDefLocales extends Nodo {
    Nodo defLocal;
    Nodo defLocales;

    public NDefLocales(Sintactico pila) {
        pila.pop();//Estado
        this.defLocales = pila.pop().getNodo();

        pila.pop();//Estado
        this.defLocal = pila.pop().getNodo();

        this.simbolo = "DefLocales";
        this.sig = null;
    }

    @Override
    public void muestra(Context context) {
        obtieneSangria();
        if(defLocales == null) {
            Log.e("Semantico", this.sangria + "<DefLocales>");
            ((MainActivity) context).muestraArbol(this.sangria + "<DefLocales>");
        }

        this.tamSangria++;
        if(defLocales != null)
            defLocales.muestra(context);
        if(defLocal != null)
            defLocal.muestra(context);
        this.tamSangria--;

        if(sig != null)
            sig.muestra(context);
    }
}
