package com.example.asator.lexer.Sintactico;

import android.content.Context;
import android.util.Log;

import com.example.asator.lexer.MainActivity;

/**
 * Created by mfrausto on 11/27/16.
 */

public class NExpresion extends Nodo{
    public NExpresion izq, der;
    public Nodo termino;

    public NExpresion(){
        izq = null;
        der = null;
    }

    public NExpresion(Nodo termino){
        this.termino = termino;
        this.simbolo = termino.getSimbolo();
    }

    public NExpresion(NExpresion izq, NExpresion der){
        this.izq = izq;
        this.der = der;
        this.simbolo = "Expresion";
    }

    public String guardaArbol(){
        return "";
    }

    @Override
    public void muestra(Context context) {
        obtieneSangria();
        if(termino == null) {
            Log.e("Semantico", this.sangria + "<Expresion> ::= " + this.simbolo);
            ((MainActivity)context).muestraArbol(this.sangria + "<Expresion> ::= " + this.simbolo);
        }else{
            Log.e("Semantico", this.sangria + "<Expresion>");
            ((MainActivity)context).muestraArbol(this.sangria + "<Expresion>");
        }


        this.tamSangria++;
        if(termino != null)
            termino.muestra(context);
        if(izq != null)
            izq.muestra(context);
        if(der != null)
            der.muestra(context);

        this.tamSangria--;

        if(sig != null)
            sig.muestra(context);
    }
}
