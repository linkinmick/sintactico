package com.example.asator.lexer.Sintactico;

import android.content.Context;
import android.util.Log;

import com.example.asator.lexer.MainActivity;

/**
 * Created by mfrausto on 11/27/16.
 */

public class NListaParam extends Nodo{
    NIdentificador identificador;
    NTipo tipo;

    public NListaParam(Sintactico pila, Nodo sig){
        this.identificador = new NIdentificador(pila, null);
        this.tipo = new NTipo(pila);
        pila.pop();//Estado
        pila.pop();//Coma
        this.sig = sig;
        this.simbolo = "ListaParam";
    }

    @Override
    public void muestra(Context context) {
        obtieneSangria();
        Log.e("Semantico", this.sangria+"<ListaParam>");
        ((MainActivity)context).muestraArbol(this.sangria+"<ListaParam>");

        this.tamSangria++;
        tipo.muestra(context);
        identificador.muestra(context);
        this.tamSangria--;

        if(sig != null)
            sig.muestra(context);
    }
}
