package com.example.asator.lexer.Semantico;


import android.content.Context;

import com.example.asator.lexer.Sintactico.NDefFunc;
import com.example.asator.lexer.Sintactico.NDefVar;
import com.example.asator.lexer.Sintactico.NParametro;

import java.util.List;

/**
 * Created by mfrausto on 11/27/16.
 */

public abstract class AbstractTablaSimbolos {
    protected List<ElementoTabla> tabla;
    protected abstract Integer dispersion(String simbolo);
    public final Integer TAM = 211;

    public Variable varLocal;
    public Variable varGlobal;
    public Funcion funcion;
    public List<String> listaErrores;

    public AbstractTablaSimbolos(List<String> listaErrores){
        this.listaErrores = listaErrores;
    }

    public abstract void agrega(ElementoTabla elemento);
    public abstract void muestra(Context context);

    public abstract Boolean varGlobalDefinida(String simbolo);
    public abstract Boolean funcionDefinida(String simbolo  );
    public abstract Boolean varLocalDefinida(String variable, String funcion);
    public abstract void buscaIdentificador(String simbolo);
    public abstract void buscaFuncion(String simbolo);

    public abstract void agrega(NDefVar defVar);
    public abstract void agrega(NDefFunc defFunc);
    public abstract void agrega(NParametro parametro);



}
