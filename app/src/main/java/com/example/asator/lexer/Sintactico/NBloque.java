package com.example.asator.lexer.Sintactico;

import android.content.Context;
import android.util.Log;

import com.example.asator.lexer.MainActivity;

/**
 * Created by mfrausto on 11/27/16.
 */

public class NBloque extends Nodo {
    Nodo sentencias;

    public NBloque(Sintactico pila) {
        pila.pop();//Estado
        pila.pop();//}

        pila.pop();//Estado
        this.sentencias = pila.pop().getNodo();

        pila.pop();//Estado
        pila.pop();//{

        this.simbolo = "Bloque";
    }

    public void muestra(Context context) {
        obtieneSangria();
        Log.e("Semantico", this.sangria+"<Bloque>");
        ((MainActivity)context).muestraArbol(this.sangria+"<Bloque>");

        this.tamSangria++;
        if(sentencias != null)
            sentencias.muestra(context);
        this.tamSangria--;

        if(sig != null)
            sig.muestra(context);
    }
}

