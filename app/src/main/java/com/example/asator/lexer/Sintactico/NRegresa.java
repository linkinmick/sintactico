package com.example.asator.lexer.Sintactico;

import android.content.Context;
import android.util.Log;

import com.example.asator.lexer.MainActivity;

/**
 * Created by mfrausto on 11/27/16.
 */

public class NRegresa extends Nodo {
    protected NExpresion expresion;

    public NRegresa(NExpresion expresion, Nodo sig) {
        this.expresion = expresion;
        this.sig = sig;
    }

    @Override
    public void muestra(Context context) {
        obtieneSangria();
        Log.e("Semantico", this.sangria+"<Regresa>");
        ((MainActivity)context).muestraArbol(this.sangria+"<Regresa>");

        this.tamSangria++;
        expresion.muestra(context);
        this.tamSangria--;

        if(sig != null)
            sig.muestra(context);
    }
}
