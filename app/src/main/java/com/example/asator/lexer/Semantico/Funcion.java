package com.example.asator.lexer.Semantico;

import android.content.Context;
import android.util.Log;

/**
 * Created by mfrausto on 11/28/16.
 */
public class Funcion extends ElementoTabla {
    public String parametros;

    public Funcion(Character tipo, String simbolo, String parametros){
        this.simbolo = simbolo;
        this.tipo = tipo;
        this.parametros = parametros;
    }

    public Boolean esFuncion(){
        return true;
    }

    @Override
    public void muestra(Context context) {
        Log.e("Semantico", "Funcion: "+simbolo+" tipo = "+tipo+" Parametros = "+parametros);
    }
}
