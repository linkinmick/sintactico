package com.example.asator.lexer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.asator.lexer.Adaptadores.TokensAdapter;
import com.example.asator.lexer.Lexico.Lexico;
import com.example.asator.lexer.Lexico.TipoSimbolo;
import com.example.asator.lexer.Semantico.Semantico;
import com.example.asator.lexer.Sintactico.Estado;
import com.example.asator.lexer.Sintactico.NArgumentos;
import com.example.asator.lexer.Sintactico.NAsignacion;
import com.example.asator.lexer.Sintactico.NBloqFunc;
import com.example.asator.lexer.Sintactico.NBloque;
import com.example.asator.lexer.Sintactico.NCadena;
import com.example.asator.lexer.Sintactico.NDefFunc;
import com.example.asator.lexer.Sintactico.NDefLocal;
import com.example.asator.lexer.Sintactico.NDefLocales;
import com.example.asator.lexer.Sintactico.NDefVar;
import com.example.asator.lexer.Sintactico.NDefinicion;
import com.example.asator.lexer.Sintactico.NDefiniciones;
import com.example.asator.lexer.Sintactico.NEntero;
import com.example.asator.lexer.Sintactico.NExpresion;
import com.example.asator.lexer.Sintactico.NIdentificador;
import com.example.asator.lexer.Sintactico.NListaParam;
import com.example.asator.lexer.Sintactico.NLlamadaFunc;
import com.example.asator.lexer.Sintactico.NMult;
import com.example.asator.lexer.Sintactico.NOtro;
import com.example.asator.lexer.Sintactico.NParametro;
import com.example.asator.lexer.Sintactico.NPrograma;
import com.example.asator.lexer.Sintactico.NReal;
import com.example.asator.lexer.Sintactico.NSentencia;
import com.example.asator.lexer.Sintactico.NSentenciaBloque;
import com.example.asator.lexer.Sintactico.NSentencias;
import com.example.asator.lexer.Sintactico.NSuma;
import com.example.asator.lexer.Sintactico.NoTerminal;
import com.example.asator.lexer.Sintactico.Nodo;
import com.example.asator.lexer.Sintactico.Sintactico;
import com.example.asator.lexer.Sintactico.Terminal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List tokens;
    private List salida;//S
    private List arbol;//S

    private EditText edtEntrada;
    private Button btnAnalizar;
    private Button btnArbol;
    private ListView listaTokens;
    private ListView listaSintactico;//S
    private ListView listaArbol;//S
    private LinearLayout listasLayout;
    private LinearLayout labelsLayout;
    private TextView lblError;

    private TokensAdapter adapter = null;
    private TokensAdapter adapter2 = null;//S
    private TokensAdapter adapterArbol = null;//S
    private Lexico lexico;
    private Sintactico pila;

    private List<Integer> idReglas = new ArrayList<Integer>();
    private List<Integer> lonReglas = new ArrayList<Integer>();
    private List<String> nombreRegla = new ArrayList<String>();


    int[][] tablaLR;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        //Inicialización elementos de UI
        edtEntrada = (EditText) findViewById(R.id.edtEntrada);
        btnAnalizar = (Button) findViewById(R.id.btnAnalizar);
        btnArbol = (Button) findViewById(R.id.btnArbol);
        listaTokens = (ListView) findViewById(R.id.listaTokens);
        listaSintactico = (ListView) findViewById(R.id.listaSintactico);
        listaArbol = (ListView) findViewById(R.id.listaArbol);
        labelsLayout = (LinearLayout) findViewById(R.id.labelsLayout);
        listasLayout = (LinearLayout) findViewById(R.id.listasLayout);
        lblError = (TextView) findViewById(R.id.lblError);

        lexico = new Lexico();
        lexico.inicializacion();
        tokens = new ArrayList();
        arbol = new ArrayList();

        lblError.setVisibility(View.GONE);
        btnArbol.setVisibility(View.INVISIBLE);
        listaArbol.setVisibility(View.GONE);

        leeConfig(); //Llena vectores de idReglas, lonReglas, nombreRegla y tablaLR
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        String entrada = null;
        switch (item.getItemId()) {
            case R.id.fuente1:
                //Obtiene codigo fuente de archivo (asset)
                entrada = this.leeExterno("fuente1");
                break;

            case R.id.fuente2:
                //Obtiene codigo fuente de archivo (asset)
                entrada = this.leeExterno("fuente2");
                break;

            case R.id.fuente3:
                //Obtiene codigo fuente de archivo (asset)
                entrada = this.leeExterno("fuente3");
                break;

            case R.id.fuente4:
                //Obtiene codigo fuente de archivo (asset)
                entrada = this.leeExterno("fuente4");
                break;

            case R.id.action_settings:
                // User chose the "Settings" item, show the app settings UI...
                return true;
        }

        //Escribe en entrada de texto codigo fuente obtenido
        edtEntrada.setText(entrada);

        return true;
    }

    public void toggleSalidas(View view) {
        Integer visibilidad = listasLayout.getVisibility();

        if(visibilidad == View.VISIBLE){
            listasLayout.setVisibility(View.GONE);
            listaArbol.setVisibility(View.VISIBLE);
        }else {
            listasLayout.setVisibility(View.VISIBLE);
            listaArbol.setVisibility(View.GONE);
        }
    }

    public void analizaLexico(View view) {
        //Inicializa visibilidad
        labelsLayout.setVisibility(View.VISIBLE);
        listasLayout.setVisibility(View.VISIBLE);
        btnArbol.setVisibility(View.VISIBLE);
        lblError.setVisibility(View.GONE);

        //Obtiene codigo fuente de archivo (asset)
        String entrada = edtEntrada.getText().toString();

        if(entrada.isEmpty())
            return;//Sale si no hay entrada a analizar

        //Limpia variables
        tokens.clear();
        lexico.simbolo = "";

        lexico.entrada(entrada);

        while (lexico.simbolo.compareToIgnoreCase("$") != 0){
            List<String> valores = new ArrayList<>();

            Integer tipo = lexico.sigSimbolo();
            if(tipo != -1){
                valores.add(lexico.simbolo);
                valores.add(lexico.tipoAcad(lexico.tipo));
                tokens.add(valores);
            } else {
                setError("Caracter "+lexico.getC()+" no esperado", 1);
                return;
            }
        }


        if(adapter != null){
            adapter.notifyDataSetChanged();
        }else {
            adapter = new TokensAdapter(this, tokens);
            listaTokens.setAdapter(adapter);
        }

        analizaSintactico();

    }

    public void analizaSintactico(){
        //Obtiene codigo fuente de entrada de texto
        String entrada = edtEntrada.getText().toString();

        //Declaración e inicialización de variables
        pila = new Sintactico();
        salida = new ArrayList();
        int fila, columna, accion;
        boolean acetacion = false;
        Nodo nodo;
        NExpresion expresion, eizq, eder;
        NIdentificador identificador;
        Nodo sentenciaBloque;
        Nodo otro;
        String simbolo;

        salida.clear();
        arbol.clear();

        lexico.entrada(entrada);

        //Valores iniciales de pila
        pila.push(new Terminal(TipoSimbolo.PESOS, "$"));
        pila.push(new Estado(0));

        //Obtiene siguiente simbolo
        lexico.sigSimbolo();

        if(lexico.tipo == TipoSimbolo.ERROR){
            Log.e("MAIN error: No esperado", lexico.simbolo);
            setError("Caracter "+lexico.getC()+" no esperado", 1);
            return;
        }

        //Encontrar el siguiente paso a realizar
        fila = pila.top().getId();
        columna = lexico.tipo;
        accion = tablaLR[fila][columna];

        do{

            //Re-inicializa variables
            List<String> valores = new ArrayList<>();
            List<String> valsPila = new ArrayList<>();
            String strPila;
            nodo = sentenciaBloque = otro = null;
            identificador = null;
            expresion = eder = eizq = null;
            simbolo = null;

            //Mostramos el contenido de la pila, entrada y accion
            strPila = pila.muestra();
            Log.d("MAIN entrada:", lexico.simbolo);
            Log.d("MAIN accion: ", ((Integer)accion).toString());

            valsPila.add("PILA: ");
            valsPila.add(strPila);
            salida.add(valsPila);

            valores.add("entrada: \t"+lexico.simbolo);
            valores.add("accion: "+((Integer)accion).toString());
            salida.add(valores);

            if(accion > 0){//desplazamiento
                //Meter tipo y accion
                pila.push(new Terminal(lexico.tipo, lexico.simbolo));
                pila.push(new Estado(accion));
                lexico.sigSimbolo();
                if(lexico.tipo == TipoSimbolo.ERROR){
                    Log.e("MAIN error: No esperado", lexico.simbolo);
                    setError("Caracter "+lexico.getC()+" no esperado", 1);
                    return;
                }
            }else if(accion < 0){ //reduccion
                int idx = Math.abs(accion +2);

                //Regla a aplicar
                Log.i("MAIN reduccion:", "Regla a aplicar: "+idx+" - "+nombreRegla.get(idx));

                //Elementos a reducir
                Log.i("MAIN reduccion:", "Elementos a reducir: "+lonReglas.get(idx));

                List<String> reduccion = new ArrayList<>();
                reduccion.add("-Reducción: ");
                reduccion.add("Regla: "+nombreRegla.get(idx)+"\t\telementos a reducir: "+lonReglas.get(idx));
                salida.add(reduccion);

                switch (idx){
                    case 0:
                        nodo = new NPrograma(pila);
                        break;

                    case 2://Definiciones
                        nodo = new NDefiniciones(pila);
                        break;

                    case 3://Definicion
                        pila.pop();//Estado
                        nodo = pila.pop().getNodo();
                        break;

                    case 4://Definicion
                        nodo = new NDefinicion(pila);
                        break;

                    case 5://DefVar
                        nodo = new NDefVar(pila, null);
                        break;

                    case 7://ListaVar
                        pila.pop();//Estado
                        //pila.pop().getSimbolo();//ListaVar

                        nodo = new NIdentificador(pila, pila.pop().getNodo());

                        pila.pop();//Estado
                        pila.pop().getSimbolo();//coma
                        break;

                    case 8://DefFunc
                        nodo = new NDefFunc(pila);
                        break;

                    case 10://Parametros
                        pila.pop();//Estado
                        nodo = new NParametro(pila, pila.pop().getNodo());
                        break;

                    case 12://ListaParam
                        pila.pop();//Estado
                        nodo = new NListaParam(pila, pila.pop().getNodo());
                        break;

                    case 13://BloqFunc
                        pila.pop();//Estado
                        pila.pop();//{

                        pila.pop();//Estado
                        nodo = new NBloqFunc(pila.pop().getNodo());

                        pila.pop();//Estado
                        pila.pop();//}
                        break;

                    case 15://DefLocales
                        nodo = new NDefLocales(pila);
                        break;

                    case 16://DefLocal
                        pila.pop();//Estado
                        nodo = new NDefLocal(pila.pop().getNodo(), null);
                        break;

                    case 17://DefLocal
                        pila.pop();//Estado
                        nodo = new NDefLocal(null, pila.pop().getNodo());
                        break;

                    case 19://Sentencias
                        nodo = new NSentencias(pila);
                        break;

                    case 20://Sentencia
                        pila.pop();//Estado
                        pila.pop();//;

                        pila.pop();//Estado
                        expresion = (NExpresion)pila.pop().getNodo();

                        pila.pop();//Estado
                        pila.pop();//=

                        identificador = new NIdentificador(pila, null);

                        nodo = new NAsignacion(identificador, (NExpresion)expresion, null);
                        break;

                    case 21://Sentencia
                        pila.pop();//Estado
                        otro = pila.pop().getNodo();

                        pila.pop();//Estado
                        sentenciaBloque = pila.pop().getNodo();

                        pila.pop();//Estado
                        pila.pop();//)

                        pila.pop();//Estado
                        expresion = (NExpresion)pila.pop().getNodo();

                        pila.pop();//Estado
                        pila.pop();//(

                        pila.pop();//Estado
                        pila.pop();//if

                        nodo = new NSentencia(expresion, sentenciaBloque, otro);
                        nodo.setSimbolo("if");
                        break;

                    case 22://Sentencia
                        pila.pop();//Estado
                        sentenciaBloque = pila.pop().getNodo();//EN realidad es Bloque, pero resusando vars

                        pila.pop();//Estado
                        pila.pop();//)

                        pila.pop();//Estado
                        expresion = (NExpresion)pila.pop().getNodo();

                        pila.pop();//Estado
                        pila.pop();//(

                        pila.pop();//Estado
                        pila.pop();//while

                        nodo = new NSentencia(expresion, sentenciaBloque, idx);
                        nodo.setSimbolo("while");
                        break;

                    case 23://Sentencia
                        pila.pop();//Estado
                        pila.pop();//;

                        pila.pop();//Estado
                        nodo = new NSentencia(pila.pop().getNodo(), idx);

                        pila.pop();//Estado
                        pila.pop();
                        nodo.setSimbolo("return");

                        break;

                    case 24://Sentencia
                        pila.pop();//Estado
                        pila.pop();//;

                        pila.pop();//Estado
                        nodo = new NSentencia(pila.pop().getNodo(), idx);
                        break;

                    case 26://Otro
                        nodo = new NOtro(pila);
                        break;

                    case 27://Bloque
                        nodo = new NBloque(pila);
                        break;

                    case 31://Argumentos
                        nodo = new NArgumentos(pila);
                        break;

                    case 35://Termino identificador
                        nodo = new NIdentificador(pila, null);
                        break;

                    case 36://Termino entero
                        pila.pop();//Estado
                        nodo = new NEntero(pila.pop().getSimbolo());
                        break;

                    case 37://Termino real
                        pila.pop();//Estado
                        nodo = new NReal(pila.pop().getSimbolo());
                        break;

                    case 38://Termino cadena
                        pila.pop();//Estado
                        nodo = new NCadena(pila.pop().getSimbolo());
                        break;

                    case 39://LlamadaFunc
                        nodo = new NLlamadaFunc(pila);
                        break;

                    case 41://SentenciaBloque
                        nodo = new NSentenciaBloque(pila);
                        break;

                    case 45://Expresion - Suma
                        pila.pop();//Estado
                        eder = (NExpresion)pila.pop().getNodo();

                        pila.pop();//Estado
                        simbolo = pila.pop().getSimbolo();//operador

                        pila.pop();//Estado
                        eizq = (NExpresion)pila.pop().getNodo();
                        nodo = new NMult("*", eizq, eder);
                        nodo.setSimbolo(simbolo);
                        break;

                    case 46://Expresion - Suma
                        pila.pop();//Estado
                        eder = (NExpresion)pila.pop().getNodo();

                        pila.pop();//Estado
                        simbolo = pila.pop().getSimbolo();//operador

                        pila.pop();//Estado
                        eizq = (NExpresion)pila.pop().getNodo();
                        nodo = new NSuma("+", eizq, eder);
                        nodo.setSimbolo(simbolo);
                        break;

                    case 47:
                    case 48:
                    case 49:
                    case 50://Expresion
                        pila.pop();//Estado
                        eder = (NExpresion)pila.pop().getNodo();

                        pila.pop();//Estado
                        simbolo = pila.pop().getSimbolo();//operador

                        pila.pop();//Estado
                        eizq = (NExpresion)pila.pop().getNodo();
                        nodo = new NExpresion(eizq,eder);
                        nodo.setSimbolo(simbolo);
                        break;

                    case 51://Expresion
                        pila.pop();//Estado
                        nodo = new NExpresion(pila.pop().getNodo());
                        break;

                    default:
                        for (int i=0; i<(lonReglas.get(idx)*2); i++){//Ciclo que itera tantas veces como elementos a reducir
                            pila.pop();//Reduce elemento
                        }
                }

                fila = pila.top().getId();
                columna = idReglas.get(idx); //no terminal
                accion = tablaLR[fila][columna];

                //transicion
                NoTerminal nt = new NoTerminal(idReglas.get(idx), nombreRegla.get(idx));
                nt.setNodo(nodo);

                pila.push(nt); //No terminal que representa a regla;
                pila.push(new Estado(accion));
            } else {//Error
                pila.pop();
                Log.e("MAIN error:", "Esperada Acción");
                setError("Esperada acción, '"+lexico.simbolo+"' no esperado", 2);
                return;
            }

            //Encontrar el siguiente paso a realizar
            fila = pila.top().getId();
            columna = lexico.tipo;
            accion = tablaLR[fila][columna];
        }while (accion != -1);

        //Mostramos el contenido de la pila, entrada y accion
        pila.muestra();
        Log.d("MAIN entrada:", lexico.simbolo);
        Log.d("MAIN accion: ", ((Integer)accion).toString());

        List<String> valores = new ArrayList<>();
        valores.add("entrada: \t"+lexico.simbolo);
        valores.add("accion: "+((Integer)accion).toString());
        salida.add(valores);

        //SEMANTICO
        if(nodo != null){
            nodo.muestra(this);//Nodo <Programa>
        }

        Semantico semantico = new Semantico();
        semantico.analiza(nodo, this);


        adapter2 = new TokensAdapter(this, salida);
        listaSintactico.setAdapter(adapter2);

    }

    public void muestraArbol(String nodo){
        List<String> valores = new ArrayList<>();
        valores.add("Arbol: ");
        valores.add(nodo);
        arbol.add(valores);

        if(adapterArbol != null){
            adapterArbol.notifyDataSetChanged();
        }else {
            adapterArbol = new TokensAdapter(this, arbol);
            listaArbol.setAdapter(adapterArbol);
        }
    }

    public String leeExterno(String nombre){
        BufferedReader br = null;
        String entrada = "";
        String nombreArchivo = nombre+".m9";
        try {
            br = new BufferedReader( new InputStreamReader(getAssets().open(nombreArchivo)));

            String linea;
            while ((linea = br.readLine()) != null) {
                entrada += linea+"\n";
            }
            br.close();
        }
        catch (IOException e) {
            e.printStackTrace();

        }

        return entrada;
    }

    public void leeConfig(){
        BufferedReader br;

        try {
            String line;
            br = new BufferedReader( new InputStreamReader(getAssets().open("compilador.lr")));
            Integer cnt = null, numLineasVectores = null, filasMatriz = null, columnasMatriz = null;

            while ((line = br.readLine()) != null) {
                if(cnt == null){ //Primera línea
                    numLineasVectores = Integer.parseInt(line);
                    cnt = 0;
                }

                else if(numLineasVectores != null){
                    if(cnt <= numLineasVectores) {
                        obtieneVectores(line.split("\\s+"));
                    } else if(cnt > numLineasVectores){
                        String[] filasColumnasMatriz = line.split("\\s+");
                        filasMatriz = Integer.parseInt(filasColumnasMatriz[0]);
                        columnasMatriz = Integer.parseInt(filasColumnasMatriz[1]);
                        tablaLR = new int[filasMatriz][columnasMatriz];
                        cnt = 0;
                        numLineasVectores = null;
                    }
                } else if(filasMatriz != null){
                    if(cnt <= filasMatriz) {
                        llenaMatriz(line.split("\\s+"), cnt-1, columnasMatriz);
                    }
                } else {

                }
                cnt++;
            }
            br.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void obtieneVectores(String[] vector){
        idReglas.add(Integer.parseInt(vector[0]));
        lonReglas.add(Integer.parseInt(vector[1]));
        nombreRegla.add(vector[2]);
    }

    public void llenaMatriz(String[] vector, Integer fila, Integer columnas){
        for (int i=0; i < columnas; i++) {
            tablaLR [fila][i] = Integer.parseInt(vector[i]);
        }
    }

    /*
    Método que muestra errores en proceso de compilación

        @param error: Mensaje de error
        @param tipo: tipo de error donde:
            1 - lexico
            2 - sintactico
            3 - semantico
            x - indefinido
     */
    public void setError(String error, Integer tipo){
        //Esconde otros elementos
        labelsLayout.setVisibility(View.GONE);
        listasLayout.setVisibility(View.GONE);
        btnArbol.setVisibility(View.GONE);
        listaArbol.setVisibility(View.GONE);

        //Establece mensaje de error
        String mensajeError = "Error ["+getTipoError(tipo)+"]: "+error;
        lblError.setText(mensajeError);

        //Muestra elemento de error
        lblError.setVisibility(View.VISIBLE);
    }

    public String getTipoError(Integer tipo){
        switch (tipo){
            case 1:
                return "Lexico";

            case 2:
                return "Sintactico";

            case 3:
                return "Semantico";

            default:
                return "Indefinido";
        }
    }





}

