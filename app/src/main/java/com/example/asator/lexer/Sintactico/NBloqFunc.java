package com.example.asator.lexer.Sintactico;

import android.content.Context;
import android.util.Log;

import com.example.asator.lexer.MainActivity;

/**
 * Created by mfrausto on 11/27/16.
 */

public class NBloqFunc extends Nodo {
    Nodo defLocales;

    public NBloqFunc(Nodo defLocales){
        this.defLocales = defLocales;
        this.simbolo = "BloqFunc";
        this.sig = null;
    }

    @Override
    public void muestra(Context context) {
        obtieneSangria();
        Log.e("Semantico", this.sangria+"<BloqFunc>");
        ((MainActivity)context).muestraArbol(this.sangria+"<BloqFunc>");

        this.tamSangria++;
        if(defLocales != null)
            defLocales.muestra(context);
        this.tamSangria--;

        if(sig != null)
            sig.muestra(context);
    }
}
