package com.example.asator.lexer.Sintactico;

import android.content.Context;
import android.util.Log;

import com.example.asator.lexer.MainActivity;

/**
 * Created by mfrausto on 11/27/16.
 */

public class NSentenciaBloque extends Nodo {
    Nodo bloque;

    public NSentenciaBloque(Sintactico pila) {
        pila.pop();//Estado
        this.bloque = pila.pop().getNodo();

        this.simbolo = "SentenciaBloque";
    }

    public void muestra(Context context) {
        obtieneSangria();
        Log.e("Semantico", this.sangria+"<SentenciaBloque>");
        ((MainActivity)context).muestraArbol(this.sangria+"<SentenciaBloque>");

        this.tamSangria++;
        if(bloque != null)
            bloque.muestra(context);
        this.tamSangria--;

        if(sig != null)
            sig.muestra(context);
    }
}
