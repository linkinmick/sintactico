package com.example.asator.lexer.Sintactico;

import android.content.Context;
import android.util.Log;

import com.example.asator.lexer.MainActivity;

/**
 * Created by mfrausto on 11/27/16.
 */

public class NEntero extends NExpresion {
    public NEntero(String simbolo) {
        this.simbolo = simbolo;
        this.sig = null;
    }

    @Override
    public void muestra(Context context) {
        obtieneSangria();
        Log.e("Semantico", this.sangria+"<Entero> ::= "+this.simbolo);
        ((MainActivity)context).muestraArbol(this.sangria+"<Entero> ::= "+this.simbolo);
    }
}
