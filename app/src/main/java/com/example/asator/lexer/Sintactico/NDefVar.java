package com.example.asator.lexer.Sintactico;

import android.content.Context;
import android.util.Log;

import com.example.asator.lexer.MainActivity;

/**
 * Created by mfrausto on 11/26/16.
 */

public class NDefVar extends Nodo {
    public NTipo tipo;
    public NIdentificador listaVar;

    public NDefVar(Sintactico pila, Nodo sig){
        /*this.tipo = tipo;
        this.listaVar = listaVar;*/

        pila.pop();//Estado
        pila.pop().getSimbolo();//puntoycoma

        pila.pop();//Estado
        //pila.pop().getSimbolo();//ListaVar

        this.listaVar= new NIdentificador(pila, pila.pop().getNodo());
        this.tipo = new NTipo(pila);

        this.simbolo = "DefVar";
        this.sig = sig;
    }

    @Override
    public void muestra(Context context) {
        obtieneSangria();
        Log.e("Semantico", this.sangria+"<DefVar>");
        ((MainActivity)context).muestraArbol(this.sangria+"<DefVar>");

        this.tamSangria++;
        tipo.muestra(context);
        listaVar.muestra(context);
        this.tamSangria--;

        if(sig != null)
            sig.muestra(context);
    }

    public void validaTipos(){
        tipoDato = tipo.dameTipo();
        tablaSimbolos.agrega(this);
    }

    public String getAmbito(){
        return this.ambito;
    }
}
