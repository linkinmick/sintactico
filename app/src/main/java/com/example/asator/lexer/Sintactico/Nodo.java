package com.example.asator.lexer.Sintactico;

import android.content.Context;
import android.util.Log;

import com.example.asator.lexer.MainActivity;
import com.example.asator.lexer.Semantico.TablaSimbolos;

/**
 * Created by mfrausto on 11/20/16.
 */

public class Nodo {
    protected String simbolo;
    public static Integer tamSangria;
    public String sangria;
    public Nodo sig;
    public Context context;

    Character tipoDato;
    TablaSimbolos tablaSimbolos;
    String ambito;


    public Nodo() {
        this.sig = null;
        this.sangria = "";
        this.tamSangria = 0;
    }

    public void muestra(Context context) {
        Log.e("Nodo", this.simbolo);
        this.context = context;
        ((MainActivity)context).muestraArbol(this.simbolo);
    }

    public void muestra() {
        Log.e("Nodo", this.simbolo);
    }

    public void obtieneSangria(){
        for(int i=0; i<tamSangria; i++){
            sangria += "\t";
        }
    }

    public void validaTipos(){
        tipoDato = 'v';
        if(this.sig != null){
            sig.setAmbito("");
            sig.setTablaSimbolos(this.tablaSimbolos);
            sig.validaTipos();
        }
    }

    public String getSimbolo() {
        return simbolo;
    }

    public void setSimbolo(String simbolo) {
        this.simbolo = simbolo;
    }

    public TablaSimbolos getTablaSimbolos() {
        return tablaSimbolos;
    }

    public void setTablaSimbolos(TablaSimbolos tablaSimbolos) {
        this.tablaSimbolos = tablaSimbolos;
    }

    public String getAmbito() {
        return ambito;
    }

    public void setAmbito(String ambito) {
        this.ambito = ambito;
    }
}