package com.example.asator.lexer.Sintactico;

import android.content.Context;
import android.util.Log;

import com.example.asator.lexer.MainActivity;

/**
 * Created by mfrausto on 11/26/16.
 */

public class NPrograma extends Nodo {
    Nodo definiciones;
    Context context;

    public NPrograma(Sintactico pila){
        super();
        pila.pop();//Estado
        this.definiciones = pila.pop().getNodo();
        this.sig = this.definiciones;
    }

    @Override
    public void muestra(Context context) {
        /*Log.e("Semantico", "<Programa>");
        this.context = context;
        ((MainActivity)context).muestraArbol("<Programa>");

        this.tamSangria++;*/
        this.definiciones.muestra(context);
        //this.tamSangria--;
    }
}
