package com.example.asator.lexer.Sintactico;

import android.content.Context;
import android.util.Log;

import com.example.asator.lexer.MainActivity;

/**
 * Created by mfrausto on 11/27/16.
 */

public class NSentencias extends Nodo {
    Nodo sentencia;
    Nodo sentencias;

    public NSentencias(Sintactico pila) {
        pila.pop();//Estado
        this.sentencias = pila.pop().getNodo();

        pila.pop();//Estado
        this.sentencia = pila.pop().getNodo();

        this.simbolo = "Sentencias";
    }

    public void muestra(Context context) {
        obtieneSangria();
        Log.e("Semantico", this.sangria+"<Sentencias>");
        ((MainActivity)context).muestraArbol(this.sangria+"<Sentencias>");

        this.tamSangria++;
        if(sentencias != null)
            sentencias.muestra(context);
        if(sentencia != null)
            sentencia.muestra(context);
        this.tamSangria--;

        if(sig != null)
            sig.muestra(context);
    }
}
