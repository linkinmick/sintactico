package com.example.asator.lexer.Sintactico;

/**
 * Created by mfrausto on 10/29/16.
 */
public class Terminal extends ElementoPila {

    public Terminal(Integer id) {
        this.id = id;
    }

    public Terminal(Integer tipo, String simbolo) {
        this.id = tipo;
        this.tipo = tipo;
        this.simbolo = simbolo;
    }

    public Terminal(Integer tipo, String simbolo, Integer subtipo) {
        this.id = tipo;
        this.simbolo = simbolo;
        this.subtipo = subtipo;
    }

    public Boolean esTerminal(){
        return true;
    }
}