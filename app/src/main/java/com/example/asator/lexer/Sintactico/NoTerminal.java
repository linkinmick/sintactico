package com.example.asator.lexer.Sintactico;

/**
 * Created by mfrausto on 10/29/16.
 */
public class NoTerminal extends ElementoPila {

    public NoTerminal(Integer tipo, String simbolo) {
        this.id = tipo;
        this.simbolo = simbolo;
        this.tipo = tipo;
    }

    public void setNodo(Nodo nodo){
        this.nodo = nodo;
    }

    public Boolean esNoTerminal(){
        return true;
    }
}