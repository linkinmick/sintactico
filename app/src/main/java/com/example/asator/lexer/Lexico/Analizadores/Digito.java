package com.example.asator.lexer.Lexico.Analizadores;

import com.example.asator.lexer.Lexico.Lexico;
import com.example.asator.lexer.Lexico.SimbolosAceptacion;
import com.example.asator.lexer.Lexico.TipoSimbolo;

/**
 * Created by mfrausto on 8/25/16
 */
public class Digito extends Lexico implements AnalizadorToken  {

    private Lexico lexico;

    public Digito(Lexico lexico){
        this.lexico = lexico;
    }

    @Override
    public void sigueAnalizando() {
        lexico.simbolo += lexico.getC();
        lexico.setC(lexico.sigCaracter());

        if(lexico.esDigito(lexico.getC())) {
            sigueAnalizando();
        } else if (lexico.getC().equals('.')){
            lexico.simbolo += lexico.getC();
            lexico.setC(lexico.sigCaracter());
            if(lexico.esDigito(lexico.getC())){
                if(lexico.getReal() != null){ //Si siguiente caracter es digito analiza real
                    lexico.getReal().sigueAnalizando();
                }
            }else{ //Si siguiente caracter diferente de digito error
                lexico.aceptacion(TipoSimbolo.ERROR);
            }

        } else if (SimbolosAceptacion.esSimboloAceptacion(lexico.getC())
                || lexico.getC().equals('"')
                || lexico.getC().equals('<')
                || lexico.getC().equals('>')
                || lexico.getC().equals('=')
                || lexico.getC().equals('!')
                || lexico.getC().equals('|')
                || lexico.getC().equals('&')
                || lexico.esLetra(lexico.getC())
                ) {
            lexico.aceptacion(TipoSimbolo.ENTERO);
            lexico.customRetroceso(true);
        } else {
            lexico.aceptacion(TipoSimbolo.ENTERO);
            lexico.customRetroceso(false);
        }
    }
}