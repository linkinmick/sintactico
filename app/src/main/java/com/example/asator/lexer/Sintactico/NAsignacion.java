package com.example.asator.lexer.Sintactico;

import android.content.Context;
import android.util.Log;

import com.example.asator.lexer.MainActivity;

/**
 * Created by mfrausto on 11/27/16.
 */

public class NAsignacion extends Nodo {
    public NIdentificador identificador;
    public NExpresion expresion;

    public NAsignacion(NIdentificador identificador, NExpresion expresion, Nodo sig) {
        this.identificador = identificador;
        this.expresion = expresion;
        this.sig = sig;
    }

    @Override
    public void muestra(Context context) {
        obtieneSangria();
        Log.e("Semantico", this.sangria+"<Asignacion>");
        ((MainActivity)context).muestraArbol(this.sangria+"<Asignacion>");

        this.tamSangria++;
        identificador.muestra(context);
        expresion.muestra(context);
        this.tamSangria--;

        if(sig != null)
            sig.muestra(context);
    }
}
