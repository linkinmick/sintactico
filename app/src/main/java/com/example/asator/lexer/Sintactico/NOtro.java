package com.example.asator.lexer.Sintactico;

import android.content.Context;
import android.util.Log;

import com.example.asator.lexer.MainActivity;

/**
 * Created by mfrausto on 11/27/16.
 */

public class NOtro extends Nodo {
    Nodo sentenciaBloque;

    public NOtro(Sintactico pila){
        pila.pop();//Estado
        this.sentenciaBloque = pila.pop().getNodo();

        pila.pop();//Estado
        pila.pop();//else

        this.simbolo = "Otro";
    }

    public void muestra(Context context) {
        obtieneSangria();
        Log.e("Semantico", this.sangria+"<Otro>");
        ((MainActivity)context).muestraArbol(this.sangria+"<Otro>");

        this.tamSangria++;
        if(sentenciaBloque != null)
            sentenciaBloque.muestra(context);
        this.tamSangria--;

        if(sig != null)
            sig.muestra(context);
    }
}
