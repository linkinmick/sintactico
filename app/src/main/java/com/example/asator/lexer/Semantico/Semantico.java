package com.example.asator.lexer.Semantico;

import android.content.Context;
import android.util.Log;

import com.example.asator.lexer.Sintactico.Nodo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by mfrausto on 11/27/16.
 */

public class Semantico {

    protected List<String> listaErrores;
    protected Nodo arbol;

    TablaSimbolos tablaSimbolos;

    public Semantico() {
        this.tablaSimbolos = new TablaSimbolos(listaErrores);
        this.listaErrores = new ArrayList<>();
    }

    public void muestraErrores(){
        if(listaErrores.size() == 0){
            return;
        }

        Iterator<String> iterator = this.listaErrores.iterator();
        Log.e("Semantico", "Errores Semanticos");

        while (iterator.hasNext()){
            Log.e("S-Error", iterator.next());
        }

    }

    public Boolean existenErrores(){
        return listaErrores.size() > 0;
    }

    public void analiza(Nodo arbol, Context contexto){
        this.arbol = arbol;
        this.arbol.setTablaSimbolos(this.tablaSimbolos);
        this.arbol.setAmbito("");
        this.arbol.validaTipos();
        this.tablaSimbolos.muestra(contexto);
        this.muestraErrores();
    }
}
