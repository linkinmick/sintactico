package com.example.asator.lexer.Sintactico;

import android.content.Context;
import android.util.Log;

import com.example.asator.lexer.MainActivity;

/**
 * Created by mfrausto on 11/27/16.
 */

public class NParametro extends Nodo {
    protected NTipo tipo;
    protected NIdentificador identificador;

    public NParametro(Sintactico pila, Nodo sig){
        this.identificador = new NIdentificador(pila, null);
        this.tipo = new NTipo(pila);
        this.simbolo = "Parametros";
        this.sig = sig;
    }

    public String cadTipos(){
        String cad = "";
        cad += tipo.dameTipo();
        NParametro p = (NParametro)sig;

        while (p != null){
            cad += p.tipo.dameTipo();
            p = (NParametro)p.sig;
        }

        return cad;
    }

    @Override
    public void muestra(Context context) {
        obtieneSangria();
        Log.e("Semantico", this.sangria+"<Parametros>");
        ((MainActivity)context).muestraArbol(this.sangria+"<Parametros>");

        this.tamSangria++;
        tipo.muestra(context);
        identificador.muestra(context);
        this.tamSangria--;

        if(sig != null)
            sig.muestra(context);
    }
}
