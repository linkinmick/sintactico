package com.example.asator.lexer.Sintactico;

import android.content.Context;
import android.util.Log;

import com.example.asator.lexer.MainActivity;

/**
 * Created by mfrausto on 11/26/16.
 */

public class NDefiniciones extends Nodo {
    NDefinicion definicion;
    NDefiniciones definiciones;
    Context context;

    public NDefiniciones(Sintactico pila){
        pila.pop();//Estado
        this.definiciones = (NDefiniciones)pila.pop().getNodo();
        this.definicion = new NDefinicion(pila);
        this.sig = (this.definicion != null)?this.definicion:this.definiciones;
    }

    @Override
    public void muestra(Context context) {
        /*obtieneSangria();
        Log.e("Semantico", this.sangria+"<Definiciones>");
        ((MainActivity)context).muestraArbol(this.sangria+"<Definiciones>");

        this.tamSangria++;*/
        if(this.definicion != null){
            this.definicion.muestra(context);
        }
        if(this.definiciones != null){
            this.definiciones.muestra(context);
        }
        //this.tamSangria--;
    }
}
