package com.example.asator.lexer.Semantico;

import android.content.Context;
import android.util.Log;

import com.example.asator.lexer.Sintactico.ElementoPila;
import com.example.asator.lexer.Sintactico.NDefFunc;
import com.example.asator.lexer.Sintactico.NDefVar;
import com.example.asator.lexer.Sintactico.NIdentificador;
import com.example.asator.lexer.Sintactico.NParametro;
import com.example.asator.lexer.Sintactico.Nodo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by mfrausto on 11/28/16.
 */
public class TablaSimbolos extends AbstractTablaSimbolos{

    public TablaSimbolos(List<String> listaErrores) {
        super(listaErrores);
        this.tabla = new ArrayList<>();
    }

    public void agregaVariable(NIdentificador iden, Character tipo, String ambito){
        ElementoTabla elem;

        elem = new Variable(tipo, iden.getSimbolo(), ambito);
        agrega(elem);

        if(iden.sig != null){
            agregaVariable((NIdentificador)iden.sig, tipo, ambito);
        }
    }

    @Override
    public void agrega(NDefVar defVar) {
        Character tipo = defVar.tipo.dameTipo();
        NIdentificador p = defVar.listaVar;
        ElementoTabla elem;

        elem = new Variable(tipo, p.getSimbolo(), defVar.getAmbito());
        if(elem.esVarLocal()){
            /*if(varLocalDefinida((Variable)elem, null)){

            }*/
        } else {

        }
        agrega(elem);

        if(p.sig != null){
            agregaVariable((NIdentificador)p.sig, tipo, defVar.getAmbito());
        }

    }



    @Override
    public void agrega(NDefFunc defFunc) {

    }

    @Override
    public void agrega(NParametro parametro) {

    }

    @Override
    protected Integer dispersion(String simbolo) {
        Integer h = 0, g;
        Character c;
        Character alfa=4;

        for (int i=0; i<simbolo.length(); i++){
            c = simbolo.charAt(i);
            h += alfa*c;
            h = h % this.TAM;
        }

        return h%TAM;
    }


    @Override
    public void agrega(ElementoTabla elemento) {
        Integer ind = dispersion(elemento.simbolo);
        tabla.add(elemento);
    }
    @Override
    public void muestra(Context context) {
        Iterator<ElementoTabla> iterator = this.tabla.iterator();
        ElementoTabla elem;

        Log.e("Semantico", "*** Tabla de Simbolos ***");

        int linea = 0;
        if(tabla.size() > 0){
            while (iterator.hasNext()){
                elem = iterator.next();
                Log.e("Semantico", "Lista: "+linea);
                elem.muestra(context);
                linea++;
            }
        }

    }

    @Override
    public Boolean funcionDefinida(String funcion) {
        Integer ind = dispersion(funcion);
        Iterator<ElementoTabla> iterator = this.tabla.iterator();
        ElementoTabla elem;

        while (iterator.hasNext()){
            elem = iterator.next();
            if(elem.esFuncion()){
                if (elem.simbolo.compareTo(funcion) == 0){
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public Boolean varGlobalDefinida(String variable) {
        Integer ind = dispersion(variable);
        Iterator<ElementoTabla> iterator = this.tabla.iterator();
        ElementoTabla elem;

        while (iterator.hasNext()){
            elem = iterator.next();
            if(elem.esVariable() && !elem.esVarLocal()){
                if (elem.simbolo.compareTo(variable) == 0){
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public Boolean varLocalDefinida(String variable, String funcion) {
        Integer ind = dispersion(variable);
        Iterator<ElementoTabla> iterator = this.tabla.iterator();
        ElementoTabla elem;

        while (iterator.hasNext()){
            elem = iterator.next();
            if(elem.esVariable() && elem.esVarLocal()){
                if (((Variable)elem).ambito.compareTo(funcion) == 0 && elem.simbolo.compareTo(variable) == 0){
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public void buscaIdentificador(String simbolo) {
        Integer ind = dispersion(simbolo);
        Iterator<ElementoTabla> iterator = this.tabla.iterator();
        ElementoTabla elem;

        varGlobal = null;
        varLocal = null;
        funcion = null;

        while (iterator.hasNext()){
            elem = iterator.next();
            if(elem.simbolo.compareTo(simbolo) == 0){
                if(elem.esVariable()){
                    if (elem.esVarLocal()){
                        varLocal = (Variable)elem;
                    } else {
                        varGlobal = (Variable)elem;
                    }
                } else {
                    funcion = (Funcion)elem;
                }
            }
        }
    }

    @Override
    public void buscaFuncion(String simbolo) {
        Integer ind = dispersion(simbolo);
        Iterator<ElementoTabla> iterator = this.tabla.iterator();
        ElementoTabla elem;

        varGlobal = null;
        varLocal = null;
        funcion = null;

        while (iterator.hasNext()) {
            elem = iterator.next();
            if (elem.simbolo.compareTo(simbolo) == 0 && elem.esFuncion()){
                funcion = (Funcion)elem;
                return;
            }
        }

    }
}
