package com.example.asator.lexer.Sintactico;

import android.content.Context;
import android.util.Log;

import com.example.asator.lexer.MainActivity;

/**
 * Created by mfrausto on 11/27/16.
 */

public class NDefinicion extends Nodo {
    Nodo definicion;

    public NDefinicion(Sintactico pila) {
        pila.pop();//Estado
        this.definicion = pila.pop().getNodo();
        this.simbolo = "Definicion";
        this.sig = this.definicion;
    }

    @Override
    public void muestra(Context context) {
        obtieneSangria();
        Log.e("Semantico", this.sangria+"<Definicion>");
        ((MainActivity)context).muestraArbol(this.sangria+"<Definicion>");

        this.tamSangria++;
        if(definicion != null)
            definicion.muestra(context);
        this.tamSangria--;

        if(sig != null)
            sig.muestra();
    }
}
