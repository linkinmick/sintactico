package com.example.asator.lexer.Sintactico;

import android.content.Context;
import android.util.Log;

import com.example.asator.lexer.MainActivity;

/**
 * Created by mfrausto on 11/27/16.
 */

public class NSuma extends NExpresion {
    public NSuma(String simbolo, NExpresion izq, NExpresion der){
        this.simbolo = simbolo;
        this.izq = izq;
        this.der = der;
        this.sig = null;
    }

    @Override
    public String guardaArbol() {
        return "new Suma(" + simbolo + "," + izq.guardaArbol() + ", " +  der.guardaArbol() + ") ";
    }

    @Override
    public void muestra(Context context) {
        obtieneSangria();
        Log.e("Semantico", this.sangria+"<Suma>");
        ((MainActivity)context).muestraArbol(this.sangria+"<Suma>");

        this.tamSangria++;
        izq.muestra(context);
        der.muestra(context);
        this.tamSangria--;

        if(sig != null)
            sig.muestra(context);
    }
}
