package com.example.asator.lexer.Sintactico;

/**
 * Created by mfrausto on 10/29/16.
 */
public class ElementoPila {
    String elemento;
    Integer id;
    Integer tipo;
    Integer subtipo;
    String simbolo;

    Nodo nodo;

    /*public ElementoPila(String elemento) {
        this.elemento = elemento;
    }*/

    public ElementoPila(){ }

    public void mostrar(){

    }

    /*public String getElemento() {
        return elemento;
    }*/

    public Integer getId() { return this.id; }

    public String getSimbolo() { return this.simbolo; }

    public Nodo getNodo(){ return this.nodo; }

    public Boolean esEstado(){
        return false;
    }

    public Boolean esTerminal(){
        return false;
    }

    public Boolean esNoTerminal(){
        return false;
    }
}