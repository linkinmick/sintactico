package com.example.asator.lexer.Semantico;

import android.content.Context;

/**
 * Created by mfrausto on 11/27/16.
 */

public class ElementoTabla {
    public String simbolo;
    public Character tipo;

    public Boolean esVariable(){ return false; }
    public Boolean esVarLocal(){ return false; }
    public Boolean esFuncion(){ return false; }

    public void muestra(){}

    public void muestra(Context context){}
}
