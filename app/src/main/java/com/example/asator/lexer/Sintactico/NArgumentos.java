package com.example.asator.lexer.Sintactico;

import android.content.Context;
import android.util.Log;

import com.example.asator.lexer.MainActivity;

/**
 * Created by mfrausto on 11/28/16.
 */

public class NArgumentos extends Nodo {
    NExpresion expresion;
    Nodo listaArgumentos;

    public NArgumentos(Sintactico pila) {
        pila.pop();//Estado
        this.listaArgumentos = pila.pop().getNodo();

        pila.pop();//Estado
        this.expresion = (NExpresion)pila.pop().getNodo();

        this.simbolo = "Argumentos";
    }

    public void muestra(Context context) {
        obtieneSangria();
        Log.e("Semantico", this.sangria+"<Argumentos>");
        ((MainActivity)context).muestraArbol(this.sangria+"<Argumentos>");

        this.tamSangria++;
        if(expresion != null)
            expresion.muestra();
        if(listaArgumentos != null)
            listaArgumentos.muestra();
        this.tamSangria--;

        if(sig != null)
            sig.muestra(context);
    }
}
