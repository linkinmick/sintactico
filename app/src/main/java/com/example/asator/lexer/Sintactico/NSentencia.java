package com.example.asator.lexer.Sintactico;

import android.content.Context;
import android.util.Log;

import com.example.asator.lexer.MainActivity;

/**
 * Created by mfrausto on 11/27/16.
 */

public class NSentencia extends Nodo {
    Nodo identificador;
    Nodo expresion;
    Nodo bloque;
    Nodo sentenciaBloque;
    Nodo otro;
    Nodo valorRegresa;
    Nodo llamadaFuncion;

    public NSentencia(Nodo p1, Integer reglaId){
        if(reglaId == 23){
            this.valorRegresa = p1;
            this.simbolo = "return";
        }if(reglaId == 24){
            this.llamadaFuncion = p1;
            this.simbolo = "llamadaFuncion";
        }

    }

    public NSentencia(Nodo p1, Nodo p2, Integer reglaId) {
        if(reglaId == 20){
            this.identificador = p1;
            this.expresion = p2;
        }if(reglaId == 22){
            this.expresion = p1;
            this.bloque = p2;
        }
        this.simbolo = "Sentencia";
    }

    public NSentencia(Nodo expresion, Nodo sentenciaBloque, Nodo otro){
        this.expresion = expresion;
        this.sentenciaBloque = sentenciaBloque;
        this.otro = otro;
        this.simbolo = "Sentencia";
    }

    public void muestra(Context context) {
        obtieneSangria();
        Log.e("Semantico", this.sangria+"<Sentencia> ::= "+this.simbolo);
        ((MainActivity)context).muestraArbol(this.sangria+"<Sentencia> ::= "+this.simbolo);

        this.tamSangria++;
        if(identificador != null)
            identificador.muestra(context);
        if(expresion != null)
            expresion.muestra(context);
        if(bloque != null)
            bloque.muestra(context);
        if(sentenciaBloque != null)
            sentenciaBloque.muestra(context);
        if(otro != null)
            otro.muestra(context);
        if(valorRegresa != null)
            valorRegresa.muestra(context);
        if(llamadaFuncion != null)
            llamadaFuncion.muestra(context);
        this.tamSangria--;

        if(sig != null)
            sig.muestra(context);
    }
}
