package com.example.asator.lexer.Sintactico;


import java.util.Stack;

/**
 * Created by asator on 8/29/16.
 */
public abstract class AbstractSintactico {
    public Stack<ElementoPila> lista = new Stack<>();

    public abstract void push(ElementoPila x);
    public abstract ElementoPila top();
    public abstract ElementoPila pop();
    public abstract String muestra();

}
