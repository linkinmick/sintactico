package com.example.asator.lexer.Sintactico;

import android.content.Context;
import android.util.Log;

import com.example.asator.lexer.MainActivity;

/**
 * Created by mfrausto on 11/27/16.
 */

public class NDefFunc extends Nodo{
    protected NTipo tipo;
    protected NIdentificador identificador;
    protected NParametro parametros;
    protected Nodo bloqueFunc;

    public NDefFunc(Sintactico pila){
        pila.pop();//Estado
        this.bloqueFunc = pila.pop().getNodo();

        pila.pop();//Estado
        pila.pop();//)

        pila.pop();//Estado
        this.parametros = (NParametro)pila.pop().getNodo();

        pila.pop();//Estado
        pila.pop();//(

        this.identificador = new NIdentificador(pila, null);

        this.tipo = new NTipo(pila);

        this.simbolo = "DefFunc";
        this.sig = null;
    }

    @Override
    public void muestra(Context context) {
        obtieneSangria();
        Log.e("Semantico", this.sangria+"<DefFunc>");
        ((MainActivity)context).muestraArbol(this.sangria+"<DefFunc>");

        this.tamSangria++;
        tipo.muestra(context);
        identificador.muestra(context);
        if(parametros != null)
            parametros.muestra(context);
        if(bloqueFunc != null)
            bloqueFunc.muestra(context);
        this.tamSangria--;

        if(sig != null)
            sig.muestra(context);
    }
}
