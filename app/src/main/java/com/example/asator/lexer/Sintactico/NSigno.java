package com.example.asator.lexer.Sintactico;

import android.content.Context;
import android.util.Log;

import com.example.asator.lexer.MainActivity;

/**
 * Created by mfrausto on 11/27/16.
 */

public class NSigno extends NExpresion {
    public NSigno(String simbolo, NExpresion izq){
        this.simbolo = simbolo;
        this.izq = izq;
        this.sig = null;
    }

    @Override
    public void muestra(Context context) {
        obtieneSangria();
        Log.e("Semantico", this.sangria+"<Signo>");
        ((MainActivity)context).muestraArbol(this.sangria+"<Signo>");

        this.tamSangria++;
        izq.muestra(context);
        this.tamSangria--;

        if(sig != null)
            sig.muestra(context);
    }
}
