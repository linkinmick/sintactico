package com.example.asator.lexer.Semantico;

import android.content.Context;
import android.util.Log;

/**
 * Created by mfrausto on 11/28/16.
 */
public class Variable extends ElementoTabla {
    protected Boolean local;
    public String ambito;

    public Variable(Character tipo, String simbolo, String ambito) {
        this.tipo = tipo;
        this.simbolo = simbolo;
        this.ambito = ambito;
        this.local = (this.ambito.compareTo("") != 0);
    }

    public Boolean esVarLocal(){
        return this.local;
    }

    public Boolean esVariable(){
        return true;
    }

    @Override
    public void muestra(Context context) {
        Log.e("Semantico", "Variable: "+simbolo+" tipo = "+tipo);

        if(local) Log.e("Semantico", "local");
        else Log.e("Semantico", "global");
    }
}
