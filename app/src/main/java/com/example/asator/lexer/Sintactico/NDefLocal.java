package com.example.asator.lexer.Sintactico;

import android.content.Context;
import android.util.Log;

import com.example.asator.lexer.MainActivity;

/**
 * Created by mfrausto on 11/27/16.
 */

public class NDefLocal extends Nodo {
    Nodo defVar;
    Nodo sentencia;

    public NDefLocal(Nodo defVar, Nodo sentencia) {
        this.defVar = defVar;
        this.sentencia = sentencia;
        this.simbolo = "DefLocal";
        this.sig = null;
    }

    @Override
    public void muestra(Context context) {
        obtieneSangria();
        Log.e("Semantico", this.sangria+"<DefLocal>");
        ((MainActivity)context).muestraArbol(this.sangria+"<DefLocales>");

        this.tamSangria++;
        if(defVar != null)
            defVar.muestra(context);
        if(sentencia != null)
            sentencia.muestra(context);
        this.tamSangria--;

        if(sig != null)
            sig.muestra(context);
    }
}
