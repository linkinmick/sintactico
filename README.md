# Compilador Android

Compilador que corre sobre plataformas Android

## Introducción

Estas instrucciones son para montar y correr el proyecto en local

### Prerrequisitos

* git 2.10.2 o superior
* Android Studio 2.2.2 o superior
* Android Studio Build tools version:  4.0.1
* SDK Platforms:

* *  4.0
* *  4.4
* *  7.0

* Dispositivo con android 4.4 o superior o dispositivo virtual [Genymotion](https://docs.genymotion.com/Content/Home.htm) corriendo 

### Instalación

Crear carpeta donde se clonará el proyecto y clonar el proyecto en la carpeta creada.

Ej. El siguiente comando clona(descarga) el proyecto en la carpeta 'Compilador' ubicada en la carpeta por defecto de proyectos dentro del directorio home del sistema
```
git clone https://linkinmick@bitbucket.org/linkinmick/sintactico.git /home/AndroidStudioProjects/Compilador
```


## Ejecutar
* Abrir proyecto con Android Studio
* Conectar dispositivo android o correr dispositivo virtual
* Correr aplicación:

![run_menu.png](https://bitbucket.org/repo/5Lk64g/images/2840500695-run_menu.png)

### Salidas adicionales a consola

Para ver las salidas adicionales a consola hay que abrir el apartado 'Android Monitor'
![android_monitor.png](https://bitbucket.org/repo/5Lk64g/images/770098126-android_monitor.png)


## Uso y distribución
Este proyecto puede ser usado y modificado libremente siempre y cuando se den los creditos correspondientes de autoría.
**
Estrictamente prohibido lucrar con este software o cualquier producto derivado de una modificación de éste.**